package inc.lennon.controller;

import org.springframework.stereotype.Controller;

@Controller
public class IndexController {
	
	/*
	 * 
	 */
	private String magicBean = "lennon";
	private String aplicationName = "template-spring-jsf";
	
	public String getMagicBean() {
		return magicBean;
	}

	public void setMagicBean(String magicBean) {
		this.magicBean = magicBean;
	}

	public String getAplicationName() {
		return aplicationName;
	}

	public void setAplicationName(String aplicationName) {
		this.aplicationName = aplicationName;
	}
	
}
