package inc.lennon.controller;

import inc.lennon.model.Genre;

import org.springframework.stereotype.Controller;

@Controller
public class GenreController {
	
	private Genre genre = new Genre();

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
}
