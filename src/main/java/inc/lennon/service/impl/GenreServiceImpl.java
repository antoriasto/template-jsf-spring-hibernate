package inc.lennon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import inc.lennon.dao.impl.GenreDao;
import inc.lennon.model.Genre;
import inc.lennon.service.CommonService;

@Service
@Transactional
public class GenreServiceImpl implements CommonService<Genre> {
	
	@Autowired
	private GenreDao genreDao;
	
	@Override
	public Genre save(Genre model) {
		return genreDao.save(model);
	}

	@Override
	public Genre delete(Genre model) {
		return genreDao.delete(model);
	}

	@Override
	public Genre findById(Long id) {
		return genreDao.findById(id);
	}

	@Override
	public List<Genre> findAllData() {
		return genreDao.findAllData();
	}

}
