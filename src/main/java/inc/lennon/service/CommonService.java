package inc.lennon.service;

import java.util.List;

public interface CommonService<T> {
	T save(T model);
	T delete(T model);
	T findById(Long id);
	List<T> findAllData();
}
